package org.abbtech.practice;

import org.abbtech.practice.model.UserTaskEntity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.abbtech.practice.repository.UserTaskRepository;

@SpringBootApplication
public class PracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeApplication.class, args);

    }

}
