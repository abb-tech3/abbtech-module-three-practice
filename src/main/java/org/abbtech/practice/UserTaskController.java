package org.abbtech.practice;


import org.abbtech.practice.dto.TaskDTO;
import org.abbtech.practice.model.UserTaskEntity;
import org.abbtech.practice.repository.impl.UserTaskRepositoryImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usertask")
public class UserTaskController {

    private final UserTaskRepositoryImpl userTaskRepository;
    UserTaskController(UserTaskRepositoryImpl userTaskRepository){
        this.userTaskRepository=userTaskRepository;
    }

    @GetMapping("/getAllTasks")
    public List<UserTaskEntity> getAllTasks(){
       return userTaskRepository.getAllTask();
    }

    @GetMapping("/getTaskById/{task_id}")
    public Optional<UserTaskEntity> getTaskById(@PathVariable("task_id") Long task_id){
        return userTaskRepository.getTaskById(task_id);
    }

    @PostMapping("/saveTask")
    public void saveTask(@RequestBody TaskDTO task){
        var newTask=new UserTaskEntity(task.task(),task.user_id());
        userTaskRepository.saveTask(newTask);
    }

    @PostMapping("/createTasks")
    public void createTasks(@RequestBody List<TaskDTO> taskDTOList){
        List<UserTaskEntity> tasks=taskDTOList.stream().map(taskDTO -> new UserTaskEntity(taskDTO.task_id(),taskDTO.task(),taskDTO.user_id())).toList();
        userTaskRepository.createTasks(tasks);
    }

    @DeleteMapping("/deleteTask/{task_id}")
    public void deleteTask(@PathVariable("task_id") Long task_id){
        userTaskRepository.deleteTasks(task_id);
    }
}
