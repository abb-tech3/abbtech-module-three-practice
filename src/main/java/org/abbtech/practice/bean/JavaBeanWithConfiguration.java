package org.abbtech.practice.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaBeanWithConfiguration {

    @Bean("Student1")
    public Student getStudent1(){
        Student student=new Student();
        student.name="Value1";
        return student;
    }

    @Bean("Student2")
    public Student getStudent2(){
        Student student=new Student();
        student.name="Value2";
        return student;
    }


}
