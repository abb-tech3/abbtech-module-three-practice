package org.abbtech.practice.calculation;

import org.springframework.context.annotation.Configuration;

@Configuration
public interface CalculatorService {
    int multiply(int a, int b);

    double division(int a, int b);

    int addition(int a, int b);

    int subtract(int a, int b);



}
