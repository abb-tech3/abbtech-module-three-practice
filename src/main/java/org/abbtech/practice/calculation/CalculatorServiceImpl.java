package org.abbtech.practice.calculation;

import org.springframework.context.annotation.Configuration;

@Configuration
public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public double division(int a, int b) {
        return (double) a / b;
    }

    @Override
    public int addition(int a, int b) {
        return a + b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

}
