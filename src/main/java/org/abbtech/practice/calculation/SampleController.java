package org.abbtech.practice.calculation;


import org.abbtech.practice.calculation.ApplicationService;
import org.abbtech.practice.calculation.CalculationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sample")
public class SampleController {

    @Autowired
    ApplicationService applicationService;


    @PostMapping("/post")
    public int samplePost(@RequestBody CalculationDTO calculationDTO){
        String operation= calculationDTO.operation();
        int var1=calculationDTO.var1();
        int var2=calculationDTO.var2();
        switch (operation){
            case "+":
                return applicationService.addition(var1,var2);
            case "-":
                return applicationService.subtract(var1,var2);
            case "*":
                return applicationService.multiply(var1,var2);
            case "/":
                return (int) applicationService.division(var1,var2);
            default:
                return 0;
        }

    }
}
