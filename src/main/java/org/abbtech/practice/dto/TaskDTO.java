package org.abbtech.practice.dto;

public record TaskDTO(Long task_id,String task,Long user_id) {
}
