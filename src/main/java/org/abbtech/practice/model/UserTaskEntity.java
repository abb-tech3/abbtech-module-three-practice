package org.abbtech.practice.model;

public class UserTaskEntity {
    private Long task_id;
    private String task;
    private Long user_id;
    private String username;

    public UserTaskEntity(Long task_id,String task,Long user_id){
        this.task_id=task_id;
        this.task=task;
        this.user_id=user_id;
    }

    public UserTaskEntity(Long user_id,String username,String task,Long task_id){
        this.user_id=user_id;
        this.username=username;
        this.task=task;
        this.task_id=task_id;
    }

    public UserTaskEntity(String task, Long user_id) {
        this.task=task;
        this.user_id=user_id;
    }


    public Long getTask_id() {
        return task_id;
    }

    public void setTask_id(Long task_id) {
        this.task_id = task_id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserTaskEntity{" +
                "task_id=" + task_id +
                ", task='" + task + '\'' +
                ", user_id=" + user_id +
                ", username='" + username + '\'' +
                '}';
    }
}
