package org.abbtech.practice.repository;

import org.abbtech.practice.model.UserTaskEntity;

import java.util.List;
import java.util.Optional;


public interface UserTaskRepository {
    List<UserTaskEntity> getAllTask();

    Optional<UserTaskEntity> getTaskById(Long id);

    void saveTask(UserTaskEntity userTaskEntity);

    void createTasks(List<UserTaskEntity> userTaskEntityList);

    void deleteTasks(Long id);

}
