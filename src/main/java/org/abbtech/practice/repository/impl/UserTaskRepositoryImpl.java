package org.abbtech.practice.repository.impl;

import org.abbtech.practice.model.UserTaskEntity;
import org.abbtech.practice.repository.UserTaskRepository;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class UserTaskRepositoryImpl implements UserTaskRepository {

    private final JdbcTemplate jdbcTemplate;

    UserTaskRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<UserTaskEntity> getAllTask() {
        return jdbcTemplate.query("select u.user_id, u.username, t.task, t.task_id  from tasks t join users u on t.user_id =u.user_id;",
                ((rs, rowNum) -> new UserTaskEntity(
                        rs.getLong(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getLong(4))));
    }

    @Override
    public Optional<UserTaskEntity> getTaskById(Long id) {
        String sql = "select u.user_id, u.username, t.task, t.task_id  from tasks t join users u on t.user_id =u.user_id WHERE task_id=?";
        UserTaskEntity userTask = jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> new UserTaskEntity(
                        rs.getLong(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getLong(4)), id);
        return Optional.ofNullable(userTask);
    }

    @Override
    public void saveTask(UserTaskEntity userTaskEntity) {
        jdbcTemplate.update("""
                        INSERT INTO tasks(task,user_id) VALUES (?,?)
                        """,
                userTaskEntity.getTask(),
                userTaskEntity.getUser_id());
    }

    @Override
    public void createTasks(List<UserTaskEntity> userTaskEntityList) {
        jdbcTemplate.batchUpdate("""
                INSERT INTO tasks(task,user_id) VALUES (?,?)
                """, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, userTaskEntityList.get(i).getTask());
                ps.setLong(2, userTaskEntityList.get(i).getUser_id());
            }

            @Override
            public int getBatchSize() {
                return userTaskEntityList.size();
            }
        });
    }

    @Override
    public void deleteTasks(Long id) {
        jdbcTemplate.update("""
                DELETE FROM tasks WHERE task_id=?
                """, id);
    }
}
